var isScanStart = false;

window.addEventListener("deviceorientation", function (ev) {
	if (isScanStart) {
		if (ev.beta > 79 && ev.beta < 95) {
			$("#allign").css("display", "none");
		} else {
			$("#allign").css("display", "block");
		}

	}
});

var side_obj, front_obj;

async function UploadBodyScanFile() {
	var data = new FormData();
	console.log("upload" + front_obj + side_obj);
	data.append("frontFile", front_obj);
	data.append("sideFile", side_obj);
	data.append("gender", localStorage.getItem('isFemale'));
	data.append("height", localStorage.getItem('isHeight'));
	let tokenText = localStorage.getItem('token');
	ShowLoading();

	var dataResult = await fetch((cameraMode == 0) ? "https://lyflikeapp.bigthinx.com/webapp/target/webgl-body-measurement/front-and-side" : "https://lyflikeapp.bigthinx.com/webapp/target/webgl-body-measurement/front-and-side-static", {
	

	method: 'POST', // *GET, POST, PUT, DELETE, etc.
		mode: 'cors', // no-cors, *cors, same-origin
		credentials: 'same-origin', // include, *same-origin, omit
		headers: {
			'Authorization': tokenText,
			"Access-Control-Allow-Origin": "*"
		},
		body: data // body data type must match "Content-Type" header
	});
	console.log(dataResult);
	dataResult.clone().text().then((result) => {
		HideLoading();
		console.log(result);
		if (result.match(/body_shape/g) != null) {
			SetAlert("sccessfuly captured measurement")
		} else {
			SetAlert("please capture image correctly");

		}
	});
}

function SetAlert(message) {
	$("#alertModal").modal();
	$("#alertMsg").text(message);
}
var loadingCapture = $('#coverScreen');

function ShowLoading() {
	loadingCapture.css('display', 'block');
}

function HideLoading() {
	loadingCapture.css('display', 'none');
}

// Set the name of the hidden property and the change event for visibility
var hidden, visibilityChange; 
if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
  hidden = "hidden";
  visibilityChange = "visibilitychange";
} else if (typeof document.msHidden !== "undefined") {
  hidden = "msHidden";
  visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
  hidden = "webkitHidden";
  visibilityChange = "webkitvisibilitychange";
}
// If the page is hidden, pause the video;
// if the page is shown, play the video
function handleVisibilityChange() {
  if (document[hidden]) {
    //videoElement.pause();
	console.log("pause");
  } else if(isScanStart){
	  console.log("again play");
    camVideo.play();
  }
}

//Camera 
var scanT;
const smartphone = document.getElementById('smartphoneId');
const screenshotButton = document.querySelector('#screenshot-button');
var img = document.querySelector('#overlay-img');
const canvas = document.createElement('canvas');
var screenshot_img = document.getElementById("screenshot-img");
const camVideo = document.querySelector('#video_screen');


let shouldFaceUser = true;


let supports = navigator.mediaDevices.getSupportedConstraints();

let constraints = {
	video: true
};
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
var source = document.createElement('source');

var stop = () => camVideo.srcObject && camVideo.srcObject.getTracks().forEach(t => t.stop());

var isSupportedBro;




function isBrowserSupported() {

	if (navigator.getUserMedia)
		isSupportedBro = true;
	else
		isSupportedBro = false;

}

var scanT;
var cameraMode = -1;
var vid = document.getElementById("introVideo");
var introCount = 1;

function SelectCameraMode() {
	var struser = $("#cameraMode :selected").text();
	if (struser == "Select Body Scan Type") {
		alert("Please select camera mode.");
		return;
	}
	cameraMode = (struser == "Partner Mode") ? 0 : 1;
	introCount = 1;
	//ScanBody("front.mp4",'front')
	$('#camera_mode').css("display", "none");
	$('#overlaySlides').css("display", "block");
	$('#Slide1').css("display", "block");
	$('#audio1').trigger("play");
}

function ScanIntroduction() {

	if (introCount > 0)
		$('#Slide' + introCount).css("display", "none");
	$('#audio' + introCount).trigger("pause");
	if (introCount == 4 || introCount == 5) {
		$('#overlaySlides').css("display", "none");
		ScanBody("front.mp4", 'front');
		return;
	}
	introCount++;
	if (introCount > 3 && cameraMode == 0)
		introCount++;

	$('#Slide' + introCount).css("display", "block");
	$('#audio' + introCount).trigger("play");
}


function ScanBody(vPath, scanType) {

	scanT = scanType;
	$('#VideoDone').css('display', 'none');
	$('#videoDiv').css('display', 'block');

	vid.setAttribute('controls', '');
	vid.setAttribute("src", "assets/images/" + vPath);


	// controls autoplay muted playsinline
	document.removeEventListener("visibilityChange", (e) => {});
	vid.removeEventListener('onended', (e) => {});
	vid.onended = function () {
		if (isScanStart == false) {
			vid.pause();
			vid.removeAttribute('controls');
			vid.removeAttribute("src");
			isBrowserSupported();
			if (isSupportedBro == false) {
				$('#VideoDone').css('display', 'block');
			} else {
				$('#videoDiv').css('display', 'none');
				videoEnded();
			}
			console.log("video ended");
		}
	};

	vid.load();
	vid.oncanplay = function () {
		vid.play();
	};

	vid.onended
}

function videoEnded() {
	isScanStart = true;
	document.addEventListener(visibilityChange, handleVisibilityChange, false);

	if (isSupportedBro == true) {
		if (cameraMode == 0) {
			$('#timer-text').css('display', 'none');
			constraints.video = {
				facingMode: 'environment'
			}
		} else {
			$('#timer-text').css('display', 'true');
			constraints.video = {
				facingMode: 'user'
			}
		}


		camVideo.style.width = screen.width + 'px';
		camVideo.style.height = screen.height + 'px';
		if (scanT == "front") {
			stop();
			navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess);
			camVideo.setAttribute('autoplay', '');
			camVideo.setAttribute('muted', '');
			camVideo.setAttribute('playsinline', '');
			if (cameraMode == 0) SetCameraCaptureButton(1);
			else StartCountDown(1, function () {
				BodyScanSave(1)
			});
		} else {
			if (cameraMode == 0) SetCameraCaptureButton(2);
			else StartCountDown(2, function () {
				BodyScanSave(2)
			});
		}

	}
}

function StartCountDown(scanType, action) {
	smartphone.style.display = "block";

	if (scanType == 1) {
		captureFront();
		camVideo.style.transform = 'scale(-1,1)';
		screenshotButton.style.display = "block";

		screenshotButton.onclick = null;
		screenshotButton.onclick = camVideo.onclick = function () {
			screenshotButton.style.display = "none";
			document.getElementById('timer-text').style.display = 'block';

			TimerStopWatch(action);
		};
	} else {
		captureSide();
		screenshotButton.style.display = "none";

		camVideo.style.transform = 'scale(-1,1)';
		TimerStopWatch(action);
	}
}

function TimerStopWatch(action) {
	var timeleft = 10;
	var downloadTimer = setInterval(function () {
		timeleft--;
		document.getElementById('timer-text').innerHTML = timeleft;

		if (timeleft <= 0) {
			clearInterval(downloadTimer);
			action();
		}
	}, 1000);
}

function SetCameraCaptureButton(scanType) {
	smartphone.style.display = "block";
	screenshotButton.style.display = "block";
	document.getElementById('timer-text').style.display = 'none';
	if (scanType == 1) {
		captureFront();
		camVideo.style.transform = 'scale(1,1)';

	} else {
		captureSide();
		camVideo.style.transform = 'scale(1,1)';
	}
	screenshotButton.onclick = camVideo.onclick = function () {
		BodyScanSave(scanType);
	};
}



function BodyScanSave(scanType) {
	isScanStart = false;

	canvas.width = camVideo.videoWidth;
	canvas.height = camVideo.videoHeight;
	canvas.getContext('2d').drawImage(camVideo, 0, 0);
	var DataUri = canvas.toDataURL('image/png');
	// console.log(DataUri);
	smartphone.style.display = "none";

	fetch(DataUri)
		.then(res => res.blob())
		.then(blob => {
			const dataFile = new File([blob], (scanType == 1) ? "front.png" : "side.png", {
				type: "image/png"
			})

			if (scanType == 1) {
				front_obj = dataFile;
				stopIntro();
				if (cameraMode == 0) ScanBody('side.mp4', 'side');
				else ScanBody('selfie_side.mp4', 'side');
			} else {
				side_obj = dataFile;
				stop();
				SubmitBodyScan();
			}
		})
}

function SubmitBodyScan() {
	if (front_obj == null)
		SetAlert("Please do front scan.");
	else if (side_obj == null)
		SetAlert("Please do side scan.");
	else {
		UploadBodyScanFile();
	}
}
var stopIntro = () => vid.src && vid.src.getTracks().forEach(t => t.stop());

function handleSuccess(stream) {
	if ("srcObject" in camVideo) {
		camVideo.srcObject = stream;
	} else {
		camVideo.src = window.URL.createObjectURL(stream);
	}
}

function captureFront() {
	$('#overlay-img-front').css('display', 'block');
}

function captureSide() {
	$('#overlay-img-front').css('display', 'none');
	if (localStorage.getItem('isFemale') == 'Male') {
		img = document.getElementById("overlay-img-side-male");
		img.style.display = "block"; // ("assets/images/M_Side.png");
	} else {
		img = document.getElementById("overlay-img-side-female");
		img.style.display = "block"; //img.src = ("assets/images/F_Side.png");
	}
	if (cameraMode == 1)
		img.style.transform = 'scale(-1,1)';
	else
		img.style.transform = 'scale(1,1)';
}


function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
		vars[key] = value;
	});
	return vars;
}

window.onload = function () {

	var token = getUrlVars()["t"];
	token = token.replace('FIREBS%20','FIREBS ');
	var gender = getUrlVars()["g"];
	var height = getUrlVars()["h"];
	// var decrypted = CryptoJS.AES.decrypt(token, "Password");
	localStorage.setItem('token', token);
	localStorage.setItem('isFemale', gender);
	localStorage.setItem('isHeight', height)
	// "FIREBS " + decrypted.toString(CryptoJS.enc.Utf8)
};