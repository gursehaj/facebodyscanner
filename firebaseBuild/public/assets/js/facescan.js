async function UploadFaceFile(file_obj, URL) {
	ShowLoading();
	console.log("upload" + file_obj + URL);
	var data = new FormData();
	data.append("file", file_obj);
	data.append("gender", localStorage.getItem('isFemale'));
	try {
		//var dataResult = await postData("https://lyfsizeweb.lyfsize.me/webgl/Fitness_Configurator/modular-face", data, localStorage.getItem('token'));
		var dataResult = await postData(URL, data, localStorage.getItem('token'));

		HideLoading();
		dataResult.clone().text().then((result) => {
			console.log(result);
			if (result.match('hair_type') != null) {
				SetAlert("face has been successfuly generated");
			} else {
				SetAlert("please capture selfie correctly");
			}
		});
	} catch (error) {
		HideLoading();
		console.error(error);
	}	
}


function SetAlert(message) {
	$("#alertModal").modal();
	$("#alertMsg").text(message);
}
var loadingCapture = $('#coverScreen');
function ShowLoading() {
    loadingCapture.css('display', 'block');
}

function HideLoading() {
    loadingCapture.css('display', 'none');
}


//Camera 
var scanT;
const smartphone = document.getElementById('smartphoneId');
const screenshotButton = document.querySelector('#screenshot-button');
const img = document.querySelector('#overlay-img');
const canvas = document.createElement('canvas');
const camVideo = document.querySelector('#video_screen');
let shouldFaceUser = true;


let supports = navigator.mediaDevices.getSupportedConstraints();
console.log("FaceingMode :" + supports['facingMode']);

let constraints = {
	video: true
};
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
var source = document.createElement('source');


function ScanFace() {
	isBrowserSupported();
	if (isSupportedBro == true) {
		stop();
		constraints.video = {
			facingMode: 'user'
		}
		navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess);
		camVideo.style.transform = 'scale(-1,1)';
		screenshotButton.onclick = camVideo.onclick = function () {
			BodyScanSave();
		};
	} else {
		face_scan.click();
	}
}

var stop = () => camVideo.srcObject && camVideo.srcObject.getTracks().forEach(t => t.stop());

var isSupportedBro;

function BodyScanSave() {
	canvas.width = camVideo.videoWidth;
	canvas.height = camVideo.videoHeight;
	canvas.getContext('2d').drawImage(camVideo, 0, 0);
	var DataUri = canvas.toDataURL('image/png');
	smartphone.style.display = "none";

	fetch(DataUri)
		.then(res => res.blob())
		.then(blob => {
			const dataFile = new File([blob], "selfie.png", {
				type: "image/png"
			})


			UploadFaceFile(dataFile, "https://lyfsizeweb.lyfsize.me/webgl/target/selfie-upload");
		})
}

async function postData(url = '', data = {}, token = '') {
    ShowLoading();
    var response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Authorization': token
        },
        body: data // body data type must match "Content-Type" header
    });
    return await response; // parses JSON response into native JavaScript objects 
}
function isBrowserSupported() {

	if (navigator.getUserMedia)
		isSupportedBro = true;
	else
		isSupportedBro = false;

}

function handleSuccess(stream) {
     // Older browsers may not have srcObject
  if ("srcObject" in camVideo) {
    camVideo.srcObject = stream;
  } else {
    // Avoid using this in new browsers, as it is going away.
    camVideo.src = window.URL.createObjectURL(stream);
  }
}


	  function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
		vars[key] = value;
	});
	return vars;
}
window.onload = function () {
	var token = getUrlVars()["t"];
	var gender = getUrlVars()["g"];
	console.log(token);
	var decrypted = CryptoJS.AES.decrypt(token, "Password");
	console.log("Face APi Token :"+decrypted);
	localStorage.setItem('token', "FIREBS " + decrypted.toString(CryptoJS.enc.Utf8));	
	localStorage.setItem('isFemale', (gender=="Female")?"Female":"Male");

	ScanFace();
};